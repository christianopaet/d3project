
import { Injectable, EventEmitter } from '@angular/core';
import { CanvasDataEvent } from './canvas/canvas.component';
import { MinimapDataEvent } from './minimap/minimap.component';

@Injectable({
  providedIn: 'root'
})
export class MinimapService {

  eventEmitter: EventEmitter<MinimapDataEvent> = new EventEmitter();
  canvasDataEmitter: EventEmitter<CanvasDataEvent> = new EventEmitter();

  constructor() {}

}
