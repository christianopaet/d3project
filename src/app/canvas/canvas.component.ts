
import { Component, AfterViewInit, EventEmitter, Input, Output, ViewChild, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { MinimapService } from './../minimap.service';
import * as d3 from "d3";
import { MinimapDataEvent } from '../minimap/minimap.component';

export interface CanvasDataEvent {
  canvas: HTMLCanvasElement,
  borderSize: number,
  dx: number,
  dy: number,
  width: number,
  height: number,
  transform: any,
  selected: boolean
}

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements AfterViewInit {

  @Input('width') width: number = 640;
  @Input('height') height: number = 480;
  @ViewChild('canvasRef') canvasRef: ElementRef;

  public ctx: CanvasRenderingContext2D;
  public realCtx: CanvasRenderingContext2D;

  public canvas: any;
  public realCanvas: any;
  public points: any;
  public radius: number = 2.5;
  public isSelected: boolean = false;
  public borderSize: number = 0.4;
  public scale: number;
  public dx: number;
  public dy: number;

  constructor(private miniMap: MinimapService) {
    this.miniMap.eventEmitter.subscribe((data: MinimapDataEvent) => {
      this.realCanvas.property("__zoom", d3.zoomIdentity.scale(data.transform.k).translate(data.transform.x / data.transform.k * 2, data.transform.y / data.transform.k * 2));
      this.minimapZoomed();
    });
    this.points = d3.range(2000).map((index) => {
      return this.phyllotaxis(10, index);
    });

  }

  ngAfterViewInit() {
    this.realCanvas = d3.select('#canvas');
    this.realCtx = this.canvasRef.nativeElement.getContext('2d');

    this.dx = this.width * this.borderSize;
    this.dy = this.height * this.borderSize;

    this.canvas = d3.select('#canvas-hidden')
      .attr('width', this.width + this.dx * 2)
      .attr('height', this.height + this.dy * 2);

    this.ctx = this.canvas.node().getContext('2d');

    this.realCanvas.property("__zoom", d3.zoomIdentity.translate(-this.dx, -this.dy));
    
    this.realCanvas.call(d3.zoom()
      .scaleExtent([1 / 10, 10])
      .on("zoom", () => {
        this.zoomed();
      }));

    this.drawPoints();

  }

  toScreen() {
    var imgData = this.ctx.getImageData(this.dx, this.dy, this.width, this.height);
    this.realCtx.putImageData(imgData, 0, 0, 0, 0, this.width, this.height);
  }

  zoomed() {
    this.ctx.save();
    this.ctx.clearRect(0, 0, this.width + this.dx * 2, this.height + this.dy * 2);
    this.ctx.translate(d3.event.transform.x + this.dx, d3.event.transform.y + this.dy);
    this.ctx.scale(d3.event.transform.k, d3.event.transform.k);
    this.isSelected = false;
    this.drawPoints();
    this.ctx.restore();

    this.toScreen();
  }

  minimapZoomed() {
    this.ctx.save();
    this.ctx.clearRect(0, 0, this.width + this.dx * 2, this.height + this.dy * 2);
    this.ctx.translate(d3.event.transform.x * 2 + this.dx, d3.event.transform.y * 2 + this.dy);
    this.ctx.scale(d3.event.transform.k, d3.event.transform.k);
    this.isSelected = true;
    this.drawPoints();
    this.ctx.restore();

    this.toScreen();
  }

  drawPoints(minimap?: boolean) {
    this.ctx.beginPath();
    this.points.forEach((point) => {
      this.drawPoint(point);
    });
    this.ctx.fill();

    this.miniMap.canvasDataEmitter.emit({
      canvas: this.canvas.node(),
      borderSize: this.borderSize,
      dx: this.dx,
      dy: this.dy,
      width: this.width,
      height: this.height,
      transform: (d3.event && d3.event.transform) ? d3.event.transform : null,
      selected: this.isSelected
    });

    this.toScreen();
  }


  drawPoint(point) {
    this.ctx.moveTo(point[0] + this.radius, point[1]);
    this.ctx.arc(point[0], point[1], this.radius, 0, 2 * Math.PI);
  }

  phyllotaxis(radius, index) {
    var theta = Math.PI * (3 - Math.sqrt(5));
    var r = radius * Math.sqrt(index), a = theta * index;
    return [
      this.width / 2 + r * Math.cos(a),
      this.height / 2 + r * Math.sin(a)
    ];
  }

}
