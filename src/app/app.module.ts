
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CanvasModule } from './canvas/canvas.module';
import { MinimapModule } from './minimap/minimap.module';

import { AppComponent } from './app.component';
import { MinimapService } from './minimap.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CanvasModule,
    MinimapModule
  ],
  providers: [MinimapService],
  bootstrap: [AppComponent]
})
export class AppModule { }
