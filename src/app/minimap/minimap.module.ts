import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinimapComponent } from './minimap.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MinimapComponent],
  exports: [MinimapComponent]
})
export class MinimapModule { }
