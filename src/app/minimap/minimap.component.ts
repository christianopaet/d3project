
import { Component, OnInit, HostListener, Input, ViewChild, ElementRef, EventEmitter, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { MinimapService } from './../minimap.service';
import { CanvasDataEvent } from './../canvas/canvas.component';
import * as d3 from "d3";

export interface MinimapDataEvent {
  transform: any;
}

@Component({
  selector: 'app-minimap',
  templateUrl: './minimap.component.html',
  styleUrls: ['./minimap.component.css']
})

export class MinimapComponent implements AfterViewInit {

  @Input('width') width: number = 320;
  @Input('height') height: number = 240;
  @ViewChild('canvasRef') canvasRef: ElementRef;

  public ctx: CanvasRenderingContext2D;
  public cache: CanvasDataEvent;
  public canvas: any;
  public scale: number;
  public isSelected: boolean = false;
  public dx: number;
  public dy: number;

  constructor(private miniMap: MinimapService) {
    this.miniMap.canvasDataEmitter.subscribe((data: CanvasDataEvent) => {
      this.cache = data;
      this.draw(data);
      if (this.cache.transform) {
        this.isSelected = data.selected;
        if (this.isSelected) {
          this.canvas.property("__zoom", d3.zoomIdentity.scale(this.cache.transform.k).translate(this.cache.transform.x / this.cache.transform.k, this.cache.transform.y / this.cache.transform.k))
        } else {
          this.canvas.property("__zoom", d3.zoomIdentity.scale(this.cache.transform.k).translate(this.cache.transform.x / this.cache.transform.k / 2, this.cache.transform.y / this.cache.transform.k / 2))
        }
      }
    });
  }

  ngAfterViewInit() {
    this.ctx = this.canvasRef.nativeElement.getContext('2d');
    if (this.cache) {
      this.draw(this.cache);
    }
    this.canvas = d3.select('#minimapCanvas');
    this.canvas.property("__zoom", d3.zoomIdentity.translate(-this.dx * this.cache.width / this.width, -this.dy * this.cache.height / this.height))
    this.canvas.call(d3.zoom()
      .scaleExtent([1 / 10, 10])
      .on("zoom", () => {
        this.isSelected = true;
        this.miniMap.eventEmitter.emit({
          transform: d3.event.transform
        });
      }));
  }

  draw(ev: CanvasDataEvent) {
    console.log(ev);
    if (this.ctx) {
      this.scale = Math.max((ev.width - this.width) / ev.width, (ev.height - this.height) / ev.height);

      // this.dx = this.width * ev.borderSize * this.scale;
      // this.dy = this.height * ev.borderSize * this.scale;
      this.dx = this.width * ev.borderSize * this.scale;
      this.dy = this.height * ev.borderSize * this.scale;

      this.ctx.setTransform(1, 0, 0, 1, 0, 0);
      this.ctx.fillStyle = "lightgray";
      this.ctx.fillRect(0, 0, this.width, this.height);
      this.ctx.clearRect(this.dx, this.dy, this.width - (this.dx * 2), this.height - (this.dy * 2));
      this.ctx.drawImage(ev.canvas, 0, 0, ev.width + ev.dx * 2, ev.height + ev.dy * 2, 0, 0, this.width, this.height);
    }
  }


}
